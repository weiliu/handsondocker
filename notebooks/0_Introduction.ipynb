{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial aims to introduce basic **Docker concepts** and an overview of **Docker architecture**. \n",
    "\n",
    "We will practice basic Docker client commands, build Docker images starting from a basic Dockerfile and developing on that we will create an application consisting of 3 Docker containers communicating through a custom Docker network:\n",
    "\n",
    "1. a container `cont_faker` that generates random data and saves it to database\n",
    "2. a container `cont_postgres` that runs a database server and persists data in a Docker volume\n",
    "3. a container `cont_web` that runs a web server to read generated data from database and display it\n",
    "\n",
    "\n",
    "<img src=\"../img/3_containers.jpg\"  width=\"50%\" style=\"padding:10px;\">\n",
    "\n",
    "Finally, we will take advantage of **Compose plugin** to build, configure and manage these 3 containers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Outline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. [Docker client - Docker daemon](./1_Docker_client_daemon.ipynb)\n",
    "2. [Dockerfile, Images and Containers](2_Dockerfile_Images_Containers.ipynb)\n",
    "3. [More on Dockerfile](3_More_on_Dockerfile.ipynb)\n",
    "4. [Build Cache](4_Build_Cache.ipynb)\n",
    "5. [Data Management](5_Data_Management.ipynb)\n",
    "6. [Networking](6_Networking.ipynb)\n",
    "7. [Docker Compose](7_Docker_compose.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why use shell kernel and jupyter lab?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial requires installation of neither **Jupyter Notebook** nor **JupyterLab**. All commands in the notebooks can be executed from the command line. \n",
    "\n",
    "Using Jupyter with shell kernel provides a coherent and easy to use interface. JupyterLab is preferable to Jupyter Notebook because some commands are executed interactively and JupyterLab provides a tabbed workspace where you can view notebook, terminal, and text files side by side.\n",
    "\n",
    "<font color=gray>See [JupyterLab overview](https://jupyterlab.readthedocs.io/en/stable/getting_started/overview.html) for more info.</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Basic Jupyter Notebook / JupyterLab usage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "| Shortcut  | Effect   | \n",
    "|---|---|\n",
    "| ENTER  | Enter Edit mode  |\n",
    "| Escape  | Enter Command mode  |\n",
    "| CTRL + ENTER  | Run the cell   |\n",
    "| SHIFT + ENTER  | Run the cell and move to next cell  |\n",
    "| m  | Change cell type to markdown  |\n",
    "| y  | Change cell type to code   |\n",
    "| a  | Insert cell above   |\n",
    "| b  | Insert cell below   |\n",
    "| dd  | Delete the cell   |\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notebook color codes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(242, 242, 242); border:1px solid transparent; border-color: rgb(242, 242, 242); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Objective** and **Summary**\n",
    "\n",
    "Each notebook contains an `Objective` section at the beginning which briefly speficies the topics that will be introduced in the notebook and a `Summary` section at the end that lists the learned commands/concepts. \n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(255, 230, 230); border:1px solid transparent; border-radius: 6px; border-color: rgb(255, 230, 230); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**ATTENTION**\n",
    "\n",
    "Warns the user for possible misusage or security issues.   \n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**NOTE**\n",
    "   \n",
    "Expresses the alternative usage, additional short notice or redirects to links when a subject is not explained in the notebooks.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**Explanations**\n",
    " \n",
    "Explains the concept introduced.\n",
    "    \n",
    "The explanations that contain parts from Docker documentation are indicated with a **\\***.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(235, 235, 224); border:1px solid transparent; border-color: (235, 235, 224); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Run in your terminal**\n",
    "    \n",
    "This section contains interactive commands that need to be run in terminal. \n",
    "    \n",
    "To run commands in terminal inside JupyterLab, use the menu:\n",
    "    \n",
    "    File -> New -> Terminal\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Naming conventions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All Docker objects created in this training course are prefixed depending on the object type and suffixed with exercise number and version when it applies. \n",
    "\n",
    "`objtype_exerciseref:version`\n",
    "\n",
    "**Examples:**\n",
    "\n",
    "- Images: `img_ex1:v1`\n",
    "\n",
    "- Containers: `cont_ex1`\n",
    "\n",
    "- Volumes: `vlm_1`\n",
    "\n",
    "- Networks: `net_1`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## License"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parts of this material might contain extracts from [docker documentation](https://github.com/docker/docker.github.io) with modifications. These parts are indicated and licensed under Apache 2.0 license. See [License](../LICENSE_AL2).\n",
    "\n",
    "The training itself is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Check Docker Engine and Compose plugin installation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display the location of Docker binary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "which docker"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display Docker version:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker --version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Preferred Docker version is >= 20.10.\n",
    "\n",
    "Display the location of Compose binary:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display Compose version:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Preferred Compose version is >= 2.14."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check if Docker daemon is running:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(235, 235, 224); border:1px solid transparent; border-color: (235, 235, 224); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Run in your terminal**\n",
    "\n",
    "`$ sudo systemctl status docker.service`\n",
    "\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
