{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Build Cache"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(242, 242, 242); border:1px solid transparent; border-color: rgb(242, 242, 242); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Objective**\n",
    "\n",
    "This notebooks explains how Docker processes instructions in Dockerfile and leverage build cache in order to speed up image builds.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When a Docker image is built from a Dockerfile, Docker engine will cache the results of the build to be reused by subsequent builds. This helps avoid running the same build steps each time you build an image and thus speeds up your build significantly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Dockerfile](../exercises/ex4.1/Dockerfile) under **../exercises/ex4.1/** has the following content:\n",
    "\n",
    "```Dockerfile\n",
    "ARG BASE_IMAGE=debian:bullseye-slim\n",
    "FROM $BASE_IMAGE\n",
    "\n",
    "LABEL maintainer=\"hande.gozukan@inria.fr\" \\\n",
    "      description=\"A Dockerfile example that uses python script.\"\n",
    "\n",
    "ARG USERNAME\n",
    "ARG UID=1001\n",
    "ARG GID=1001\n",
    "\n",
    "ENV USER ${USERNAME:-appuser}\n",
    "\n",
    "WORKDIR /home/$USER\n",
    "\n",
    "COPY hello.py .\n",
    "\n",
    "RUN apt-get update && apt-get install -y \\\n",
    "    python3.9 \\\n",
    "    python3-pip\n",
    "\n",
    "RUN groupadd $USER --gid $GID && \\\n",
    "    useradd $USER --uid $UID \\\n",
    "    --gid $GID \\\n",
    "    --shell /bin/bash \\\n",
    "    --create-home \n",
    "\n",
    "USER $USER\n",
    "\n",
    "ENTRYPOINT [\"python3.9\", \"hello.py\"]\n",
    "\n",
    "CMD [\"stranger\"]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[hello.py](../exercises/ex4.1/hello.py) in the same directory:\n",
    "\n",
    "```python\n",
    "import sys\n",
    "\n",
    "name = sys.argv[1]\n",
    "\n",
    "print(f\"Hello {name} from container!\")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build Docker image and examine the outputs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_1:v1 ../exercises/ex4.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's rebuild another image from the same Dockerfile and compare the outputs with the outputs of the previous build:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_1:v2 ../exercises/ex4.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While building the image, the instructions in the Dockerfile are executed in top-down order. For each instruction Docker engine verifies if there already exists an image that corresponds to that specific instruction in the build cache. If it exists, Docker engine uses that layer from the cache. \n",
    "\n",
    "For the above build, we have made no changes to the Dockerfile. We observe `---> Using cache` lines meaning that Docker engine is using the layer from cache for that instruction.\n",
    "\n",
    "We can easily verify that the image IDs and sizes for images img_ex4_1:v1 and img_ex4_1:v2 are exactly the same. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will build a new image with the [Dockerfile](../exercises/ex4.2/Dockerfile) under **../exercises/ex4.2/**. The only difference is the line where we install the Python package [Faker](https://pypi.org/project/Faker/) `RUN pip3 install faker`.:\n",
    "\n",
    "```Dockerfile\n",
    "ARG BASE_IMAGE=debian:bullseye-slim\n",
    "FROM $BASE_IMAGE\n",
    "\n",
    "LABEL maintainer=\"hande.gozukan@inria.fr\" \\\n",
    "      description=\"A Dockerfile example that uses python script.\"\n",
    "\n",
    "ARG USERNAME\n",
    "ARG UID=1001\n",
    "ARG GID=1001\n",
    "\n",
    "ENV USER ${USERNAME:-appuser}\n",
    "\n",
    "WORKDIR /home/$USER\n",
    "\n",
    "COPY hello.py .\n",
    "\n",
    "RUN apt-get update && apt-get install -y \\\n",
    "    python3.9 \\\n",
    "    python3-pip\n",
    "\n",
    "RUN pip3 install faker\n",
    "\n",
    "RUN groupadd $USER --gid $GID && \\\n",
    "    useradd $USER --uid $UID \\\n",
    "    --gid $GID \\\n",
    "    --shell /bin/bash \\\n",
    "    --create-home \n",
    "\n",
    "USER $USER\n",
    "\n",
    "ENTRYPOINT [\"python3.9\", \"hello.py\"]\n",
    "\n",
    "CMD [\"stranger\"]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We copy `hello.py` file from [ex4.1](../exercises/ex4.1/hello.py) under ../exercises/ex4.2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cp ../exercises/ex4.1/hello.py ../exercises/ex4.2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build Docker image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_2:v1 ../exercises/ex4.2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Docker engine uses the images from build cache until `Step 12/16 : RUN pip3 install faker`. This step corresponds to the instruction that we added to the Dockerfile. From this step on Docker no more checks for existing layers in build cache and creates an image layer for each step between 12 and 16."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will modify the contents of [hello.py](../exercises/ex4.2/hello.py) under **../exercises/ex4.2** as follows:\n",
    "\n",
    "```python\n",
    "import sys\n",
    "\n",
    "from faker import Faker\n",
    "\n",
    "name = sys.argv[1]\n",
    "\n",
    "fake = Faker('fr_FR')\n",
    "\n",
    "fname = fake.name()\n",
    "faddr = fake.address()\n",
    "\n",
    "print(f\"\"\"Hello {name} from container! \n",
    "\n",
    "Here is your fake name: \n",
    "{fname} \n",
    "\n",
    "and fake address: \n",
    "{faddr}\"\"\")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build Docker image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_2:v2 ../exercises/ex4.2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have not changed the contents of the Dockerfile, however we have changed the contents of *hello.py* that is copied to the image. Starting from `Step 9/16 : COPY hello.py .` Docker stopped checking build cache and created a new image for each step."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px;background-color: rgb(224, 235, 235); padding:10px;\">\n",
    "\n",
    "To match an image from the cache, Docker:\n",
    "\n",
    "* Starts with a parent image, and checks if there is a child image that is derived from that parent image with an instruction exactly the same as the next instruction in the Dockerfile. If it does not exist, the cache is invalidated and for each following instruction Docker creates a new image.\n",
    "\n",
    "* If the instruction is exactly the same, for all instructions but `COPY` and `ADD` instructions, Docker reuses image layers from build cache.\n",
    "\n",
    "* For `COPY` and `ADD` instructions, a checksum is calculated for the files copied inside the container. This checksum is calculated using the contents and metadata of the files, except last-modified and last-accessed times.\n",
    "\n",
    "* Apart from files copied to image using `COPY` and `ADD` instructions Docker makes no control on the files inside the image. eg. `RUN apt-get update`\n",
    "\n",
    "<font color=gray>For more info on **Build Cache**, see [Dockerfile best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#leverage-build-cache).</font>\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**NOTE**\n",
    "    \n",
    "If you do not want to use cache while building the image, you can use `--no-cache=true` option with `docker image build` command.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run Docker container:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run --rm img_ex4_2:v2 Peppa"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Image layers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's check the image layers for **img_ex4_2:v1**, **img_ex4_2:v2** and the base image for both **debian:bullseye-slim** in more detail. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image history img_ex4_2:v1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image history img_ex4_2:v2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image history debian:bullseye-slim"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that two images, **img_ex4_2:v1** and **img_ex4_2:v2** have the same layer ID for the first 8 layers. This means that these two images share those layers. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sharing of layers among different images is possible through use of [copy-on-write(COW)](https://en.wikipedia.org/wiki/Copy-on-write) mechanism. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**NOTE**\n",
    "    \n",
    "From the output of the `docker image history` command, we observe that a part from the base image, only `COPY` and `RUN` instructions create layers that have size bigger than 0. (`ADD` instruction, which is not used in this [Dockerfile](../exercises/ex2/Dockerfile), also increases the size of the build.) Other instructions create a new layer, however do not influence the size of the image.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Efficient use of build cache"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Efficient use of build cache saves a lot of time for subsequent builds. \n",
    "\n",
    "In order to minimize invalidation of cache:\n",
    "\n",
    "* Only copy the necessary files in the next step\n",
    "* Put instructions that are more likely to change later in the file "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the Dockerfile under **../exercises/ex4.2/**, we can move \n",
    "\n",
    "```Dockerfile\n",
    "RUN groupadd $USER --gid $GID && \\\n",
    "    useradd $USER --uid $UID \\\n",
    "    --gid $GID \\\n",
    "    --shell /bin/bash \\\n",
    "    --create-home \n",
    "```\n",
    "\n",
    "and \n",
    "\n",
    "```Dockerfile\n",
    "RUN apt-get update && apt-get install -y \\\n",
    "    python3.9 \\\n",
    "    python3-pip\n",
    "```\n",
    "\n",
    "instructions above, as they are less likely to change. \n",
    "\n",
    "We assume that `hello.py` file is more subject to change.\n",
    "\n",
    "We can also move **description** part of the LABEL instruction below, as it might change depending on the evolution of the `hello.py` script."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Dockerfile](../exercises/ex4.3/Dockerfile) under **../exercises/ex4.3/** becomes:\n",
    "\n",
    "```Dockerfile\n",
    "ARG BASE_IMAGE=debian:bullseye-slim\n",
    "FROM $BASE_IMAGE\n",
    "\n",
    "LABEL maintainer=\"hande.gozukan@inria.fr\" \n",
    "\n",
    "ARG USERNAME\n",
    "ARG UID=1001\n",
    "ARG GID=1001\n",
    "\n",
    "ENV USER ${USERNAME:-appuser}\n",
    "\n",
    "RUN groupadd $USER --gid $GID && \\\n",
    "    useradd $USER --uid $UID \\\n",
    "    --gid $GID \\\n",
    "    --shell /bin/bash \\\n",
    "    --create-home \n",
    "    \n",
    "RUN apt-get update && apt-get install -y \\\n",
    "    python3.9 \\\n",
    "    python3-pip\n",
    "\n",
    "WORKDIR /home/$USER\n",
    "\n",
    "RUN pip3 install faker\n",
    "\n",
    "LABEL description=\"A Dockerfile example that uses python script taking advantage of faker package.\"\n",
    "\n",
    "COPY hello.py .\n",
    "\n",
    "USER $USER\n",
    "\n",
    "ENTRYPOINT [\"python3.9\", \"hello.py\"]\n",
    "\n",
    "CMD [\"stranger\"]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We copy `hello.py` file from [ex4.2](../exercises/ex4.2/hello.py) under ../exercises/ex4.3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cp ../exercises/ex4.2/hello.py ../exercises/ex4.3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_3:v1 ../exercises/ex4.3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run container:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run --rm img_ex4_3:v1 Peppa"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image history img_ex4_3:v1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will continue working on this Dockerfile in the next notebook and take advantage of build cache to create a Docker image which generates random data and persists it in a CSV file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(242, 242, 242); border:1px solid transparent; border-color: rgb(242, 242, 242); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Summary**\n",
    "\n",
    "While building the image, Docker executes each instruction in the Dockerfile in top-down order. For each instruction Docker engine verifies if there already exists an image that corresponds to that specific instruction in the build cache. If it exists, Docker engine uses that layer from the cache. \n",
    "\n",
    "To match an image from the cache, Docker:\n",
    "\n",
    "* Starts with a parent image, and checks if there is a child image that is derived from that parent image with an instruction exactly the same as the next instruction in the Dockerfile. If it does not exist, the cache is invalidated and for each following instruction Docker creates a new image.\n",
    "\n",
    "* If the instruction is exactly the same, for all instructions but `COPY` and `ADD` instructions, Docker reuses image layers from build cache.\n",
    "\n",
    "* For `COPY` and `ADD` instructions, a checksum is calculated for the files copied inside the container. This checksum is calculated using the contents and metadata of the files, except last-modified and last-accessed times.\n",
    "\n",
    "* Apart from files copied to image using `COPY` and `ADD` instructions Docker makes no control on the files inside the image. eg. `RUN apt-get update`\n",
    "\n",
    "\n",
    "Efficient use of build cache speeds up image building significantly.\n",
    "\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
