{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Docker compose"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(242, 242, 242); border:1px solid transparent; border-color: rgb(242, 242, 242); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Objective**\n",
    "\n",
    "This notebook introduces Docker compose, `compose.yaml` file format, basic Docker compose commands and demonstrates a multi-container Docker application managed by Docker compose.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In notebook [6_Networking.ipynb](6_Networking.ipynb), we have created an application that generates random data, saves it to Postgres database and views the created content through a web page. \n",
    "\n",
    "Most software applications require multiple services that communicate with each other. For a multi-container application, we need configuration for each container, run them, or stop them simultaneously. \n",
    "\n",
    "Dealing with each service one by one can be cumbersome. Docker compose adresses the problem of configuring and managing multiple containers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Docker compose**\n",
    "\n",
    "Docker compose is a tool for running multi-container Docker applications on a single host. It enables you to define and configure your services using Yet Another Markup Language (YAML) file. Running `docker compose up` command, Docker compose will build your images if necessary, create and run your containers as defined in the compose.yaml file.\n",
    "      \n",
    "   \n",
    "**Common use cases:**\n",
    "\n",
    "- Development environment\n",
    "- Automated testing environment\n",
    "- Single host deployments\n",
    "    \n",
    "Docker compose can also be used in production. <font color=gray>For more info, see [Use Compose in production](https://docs.docker.com/compose/production/).</font>\n",
    "   \n",
    "   \n",
    "<font color=gray>For more info, see [Overview of Docker Compose](https://docs.docker.com/compose/).</font>\n",
    "\n",
    "**compose.yaml file**\n",
    "\n",
    "Docker compose uses `compose.yaml` file to build and run Docker containers. `compose.yaml` file is a declarative file to define services, networks, volumes, configs and secrets. \n",
    "\n",
    "<font color=gray>For more info, see [Compose file reference](https://docs.docker.com/compose/compose-file/).</font>\n",
    "\n",
    "[*](../LICENSE_AL2)\n",
    "   \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compose.yaml file fields"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "A `compose.yaml` file can have 5 top-level elements:\n",
    "\n",
    "- version (depricated)\n",
    "- services\n",
    "- networks\n",
    "- volumes\n",
    "- configs\n",
    "- secrets\n",
    "    \n",
    "<font color=gray>For more info, see [Compose file](https://docs.docker.com/compose/compose-file/#compose-file).</font>\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[compose.yaml](../exercises/ex7.1/compose.yaml) file under **../exercises/ex7.1\n",
    "/** directory defines and configures the same application in previous notebook to be used by Docker compose:\n",
    "\n",
    "```YAML\n",
    "services:\n",
    "  db:\n",
    "    image: \"postgres:14-alpine\"\n",
    "    container_name: \"cont_postgres\"\n",
    "    restart: always\n",
    "    environment:\n",
    "      - DBPORT=5432\n",
    "      - DBNAME=postgres\n",
    "      - DBUSER=postgres\n",
    "      - POSTGRES_PASSWORD=mysecretpassword\n",
    "    volumes:\n",
    "      - vlm_faker:/var/lib/postgresql/data\n",
    "    networks:\n",
    "      - net_faker\n",
    "  app:\n",
    "    image: \"img_ex6_2:faker\"\n",
    "    container_name: \"cont_faker\"\n",
    "    restart: always\n",
    "    environment:\n",
    "      - DBHOST=db\n",
    "      - DBPORT=5432\n",
    "      - DBNAME=postgres\n",
    "      - DBUSER=postgres\n",
    "      - DBPASSWD=mysecretpassword\n",
    "    depends_on:\n",
    "      - db\n",
    "    networks:\n",
    "      - net_faker\n",
    "  web:\n",
    "    image: \"img_ex6_3:flask\"\n",
    "    container_name: \"cont_web\"\n",
    "    restart: always\n",
    "    environment:\n",
    "      - DBHOST=db\n",
    "      - DBPORT=5432\n",
    "      - DBNAME=postgres\n",
    "      - DBUSER=postgres\n",
    "      - DBPASSWD=mysecretpassword\n",
    "    ports:\n",
    "      - 80:5000\n",
    "    depends_on:\n",
    "      - db\n",
    "    networks:\n",
    "      - net_faker\n",
    "networks:\n",
    "   net_faker:\n",
    "     driver: bridge\n",
    "volumes:\n",
    "  vlm_faker:\n",
    "  ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are the commands we used to start the same 3 containers. The same is done using `compose.yaml` file with `docker compose up` command.\n",
    "\n",
    "Create network:\n",
    "\n",
    "```\n",
    "$ docker network create --driver bridge net_faker\n",
    "```\n",
    "\n",
    "Create volume:\n",
    "\n",
    "```\n",
    "$ docker volume create vlm_faker\n",
    "```\n",
    "\n",
    "Run containers:\n",
    "\n",
    "```\n",
    "$ docker container run \\\n",
    "    -d \\\n",
    "    --rm \\\n",
    "    --network net_faker \\\n",
    "    --mount source=vlm_faker,target=/var/lib/postgresql/data \\\n",
    "    --name cont_postgres -e POSTGRES_PASSWORD=mysecretpassword postgres:14-alpine\n",
    "\n",
    "\n",
    "$ docker container run \\\n",
    "    --rm \\\n",
    "    -d \\\n",
    "    --network net_faker \\\n",
    "    --name cont_faker \\\n",
    "    -e DBHOST=cont_postgres \\\n",
    "    -e DBPORT=5432 \\\n",
    "    -e DBNAME=postgres \\\n",
    "    -e DBUSER=postgres \\\n",
    "    -e DBPASSWD=mysecretpassword \\\n",
    "    img_ex6_2:faker\n",
    "\n",
    "$ docker container run \\\n",
    "    --rm \\\n",
    "    -d \\\n",
    "    --network net_faker \\\n",
    "    -e DBHOST=cont_postgres \\\n",
    "    -e DBPORT=5432 \\\n",
    "    -e DBNAME=postgres \\\n",
    "    -e DBUSER=postgres \\\n",
    "    -e DBPASSWD=mysecretpassword \\\n",
    "    -p 80:5000 \\\n",
    "    --name cont_web img_ex6_3:flask\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Services"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Services** is a top level element of compose.yaml file that comprises of (multiple) named service definitions. \n",
    "    \n",
    "A **service** is a component of an application and in the context of compose file, it represents the image of the service container and the configuration options. Each container started for that service should have the same configuration. \n",
    "    \n",
    "Configuration options of a service are given as `<key> : <value>` pairs.\n",
    "    \n",
    "A service can be configured to start many containers with the same configuration.\n",
    "\n",
    "</dev>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### **image**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Specifies the Docker image for the containers.\n",
    "\n",
    "If the image does not exist locally and no [build](#build) option is specified, Compose tries to pull it from Docker registry."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **container_name**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Specifies the name of the container initiated for the service."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **restart**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Restart option specifies the restart behavior when the service container stops. There are 4 possible values:\n",
    "\n",
    "- restart: \"no\" -> whatever the reason for which the container was stopped, it is not restarted.\n",
    "- restart: always -> whatever the reason for which the container was stopped, it is always restarted.\n",
    "- restart: on-failure -> the container is restarted if the exit code indicates an on-failure error.\n",
    "- restart: unless-stopped -> the container is restarted if it is not stopped.\n",
    "\n",
    "Default value is \"no\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **environment**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sets environment variables to be used by the container. Array or Map syntax can be used. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **ports**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Specifies ports to expose either by expressing both ports `HOST : CONTAINER`, or just the container port."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **depends_on**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Specifies dependencies between the services. The services will be started depending on dependency order. However, the services will not wait until the dependency service is ready."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **volumes**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Defines the named volumes or host paths to mount to a service.\n",
    "\n",
    "To share a volume between multiple services it should be defined as a named volume in the top level `volumes` key.\n",
    "\n",
    "A host path can be mounted to a service by only declaring in service definition, if it will be used only by a single service. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **networks**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Specifies the networks to join for the containers started for this service. Each network listed in this section should be defined in the top level `networks` element. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=gray>For more service configuration options see [Compose specification](https://docs.docker.com/compose/compose-file/#service-configuration-reference).</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Networks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Networks** is a top level element of compose.yaml file that lists the networks that needs to be created to be used in the application. \n",
    "\n",
    "Each service should define in its own `networks` configuration to which network it will connect to.\n",
    "</dev>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Volumes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Volumes** is a top level element of compose.yaml file that lists the volumes that needs to be created to be used in the application. \n",
    "\n",
    "Volumes are persistent data stores managed by Docker and can be shared between different containers.\n",
    "    \n",
    "Each service should define in its own `volumes` configuration the volumes to be mounted to the containers if necessary.\n",
    "</dev>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compose commands"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To display help for docker-compose either enter `docker compose` or `docker compose --help` command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create and run containers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`docker compose up` command is used to create and run containers. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use `docker compose up` command to create services and run containers with\n",
    "\n",
    "- docker compose flag `-f` to specify **compose.yaml** file location\n",
    "- docker compose up flag `-d` to run in background."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose -f ../exercises/ex7.1/compose.yaml up -d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List active containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All 3 containers are created and are running."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List volumes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker volume ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List networks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker network ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Inspect network:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker network inspect ex71_net_faker"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we notice that although we set volume name as **vlm_faker**, Compose created a volume with name **ex71_vlm_faker** appending prefix **ex71** -which in fact is the directory of the **compose.yaml** file- to the volume name specified. \n",
    "\n",
    "The same applies to network name, which is specified as **net_faker** however created as **ex71_net_faker**.\n",
    "\n",
    "And all created containers have names with prefix **ex71**.\n",
    "\n",
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**NOTE**\n",
    "    \n",
    "Compose uses a project name to isolate the environments from each other. If no project name is specified, Compose uses the basename of the project directory. \n",
    "\n",
    "Project name can be specified by using `-p` option or `COMPOSE_PROJECT_NAME` environment variable.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check [localhost:80](http://localhost:80)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stop services"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`docker compose stop` command is used to stop services run by Docker compose."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stop all containers and exit application:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose -f ../exercises/ex7.1/compose.yaml stop"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List volumes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker volume ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List networks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker network ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that all containers, volumes and networks created by Compose are still available."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Restart services"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`docker compose restart` command is used to restart stopped services."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose -f ../exercises/ex7.1/compose.yaml restart"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check [localhost:80](http://localhost:80)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stop and remove containers, volumes, networks, images"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`docker compose down` command is used to stop services and remove docker objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose -f ../exercises/ex7.1/compose.yaml down"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List networks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker network ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List volumes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker volume ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we see all service containers and networks defined in the `compose.yaml` file are deleted, only the volume is left. If we want to delete the volume as well, we should use `-v` flag."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's recreate the services and run:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose -f ../exercises/ex7.1/compose.yaml up -d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check [localhost:80](http://localhost:80)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compose uses the volume **ex71_vlm_faker** which already contains a database and Postgres container uses that database.\n",
    "\n",
    "Now we will rerun `docker compose down` command to remove the volume as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose -f ../exercises/ex7.1/compose.yaml down -v"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List volumes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker volume ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Build service images"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`docker compose build` command is used to build service images for services defined in the docker-compose file.\n",
    "\n",
    "For Docker compose to build a Docker image, the context and Dockerfile for the image should be specified using `build` key in the `compose.yaml` file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use [compose.yaml](../exercises/ex7.2/compose.yaml) file under **../exercises/ex7.2\n",
    "/** which contains `build` key:\n",
    "\n",
    "```YAML\n",
    "services:\n",
    "  db:\n",
    "    image: \"postgres:14-alpine\"\n",
    "    container_name: \"cont_postgres\"\n",
    "    restart: always\n",
    "    environment:\n",
    "      - DBPORT=5432\n",
    "      - DBNAME=postgres\n",
    "      - DBUSER=postgres\n",
    "      - POSTGRES_PASSWORD=mysecretpassword\n",
    "    volumes:\n",
    "      - vlm_faker:/var/lib/postgresql/data\n",
    "    networks:\n",
    "      - net_faker\n",
    "  app:\n",
    "    image: \"img_ex6_2:faker\"\n",
    "    container_name: \"cont_faker\"\n",
    "    build:\n",
    "      context: ../ex6.2/\n",
    "    restart: always\n",
    "    environment:\n",
    "      - DBHOST=db\n",
    "      - DBPORT=5432\n",
    "      - DBNAME=postgres\n",
    "      - DBUSER=postgres\n",
    "      - DBPASSWD=mysecretpassword\n",
    "    depends_on:\n",
    "      - db\n",
    "    networks:\n",
    "      - net_faker\n",
    "  web:\n",
    "    image: \"img_ex6_3:flask\"\n",
    "    container_name: \"cont_web\"\n",
    "    build:\n",
    "      context: ../ex6.3/\n",
    "    restart: always\n",
    "    environment:\n",
    "      - DBHOST=db\n",
    "      - DBPORT=5432\n",
    "      - DBNAME=postgres\n",
    "      - DBUSER=postgres\n",
    "      - DBPASSWD=mysecretpassword\n",
    "    ports:\n",
    "      - 80:5000\n",
    "    depends_on:\n",
    "      - db\n",
    "    networks:\n",
    "      - net_faker\n",
    "networks:\n",
    "   net_faker:\n",
    "     driver: bridge\n",
    "volumes:\n",
    "  vlm_faker:\n",
    "  ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will build the images for services **web** and **app**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose -f ../exercises/ex7.2/compose.yaml build"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='build'></a>\n",
    "#### **build**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Specifies configuration options for building service images from Dockerfile and context.\n",
    "\n",
    "It can be specified either as a path to build context, or as an object with `<key>: <value>` options.\n",
    "\n",
    "For more info on build see [build](https://docs.docker.com/compose/compose-file/build/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(242, 242, 242); border:1px solid transparent; border-color: rgb(242, 242, 242); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Summary**\n",
    "\n",
    "Docker compose is a tool to configure, define and manage multi-container Docker applications.\n",
    "    \n",
    "Docker compose uses `compose.yaml` file to manage services.\n",
    "\n",
    "    \n",
    "    \n",
    "Docker compose command format:\n",
    "    \n",
    "`docker compose <command> [options]`\n",
    "\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
