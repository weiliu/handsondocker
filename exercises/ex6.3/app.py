from flask import Flask, render_template

import os
import csv
import psycopg2

app = Flask(__name__)

@app.route('/')
def index():
    dbname = os.environ['DBNAME']
    dbhost = os.environ['DBHOST']
    dbport = os.environ['DBPORT']
    dbuser = os.environ['DBUSER']
    dbpasswd = os.environ['DBPASSWD']

    conn = psycopg2.connect(host=dbhost, 
                        port = dbport, 
                        database=dbname, 
                        user=dbuser, 
                        password=dbpasswd)

    # Create a cursor object
    cur = conn.cursor()
    cur.execute("SELECT * FROM fakers ;")
    data = cur.fetchall()

    clients = []
    for row in data:
        clients.append({
            "id": row[0],
            "name": row[1],
            "iban": row[2],
            "job": row[3],
            "tel": row[4]
            })
    return render_template("index.html", clients=clients)