import sys
import os

from faker import Faker
from faker.providers import bank, job, phone_number

user = os.environ['USER']
name = sys.argv[1]
count = int(sys.argv[2])

filepath = f"/home/{user}/fake_{name}.csv"

print(f"""Hello {name} from container!
I am generating {count} fake data for you.""")

faker = Faker('fr_FR')
faker.add_provider(bank)
faker.add_provider(job)
faker.add_provider(phone_number)

entry = ""

for i in range(count):
    fname = faker.name()
    fiban = faker.iban()
    fjob = faker.job()
    fphone = faker.phone_number()

    entry += f"""{name}\t{fname}\t{fiban}\t{fjob}\t{fphone}\n"""

with open(filepath, 'w') as f:
    f.write(entry)

print(f"Data generated and written to {filepath}.")