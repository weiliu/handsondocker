.. Hands-on Docker documentation master file, created by
   sphinx-quickstart on Thu Sep 17 13:09:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:gitlab_url: https://gitlab.inria.fr/hg/formations/handsondocker


.. include:: README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   notebooks/0_Introduction
   notebooks/1_Docker_client_daemon
   notebooks/2_Dockerfile_Images_Containers
   notebooks/3_More_on_Dockerfile
   notebooks/4_Build_Cache
   notebooks/5_Data_Management
   notebooks/6_Networking
   notebooks/7_Docker_compose
