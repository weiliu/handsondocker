.. contents:: **Overview**
   :depth: 3
	   
---------------
Hands-on Docker
---------------

This training aims to give a taste of Docker with a hands-on experience.
Docker concepts and its architecture will be explained in their basics.
The main objective of this training is to show a complete workflow that
could be useful to everyone. By the end of the course, participants
should have a basic understanding of the concepts and some experience
with the basic Docker commands that would enable them to assess possible
use cases for their own work. More in-depth reading is left to the
participants.

The training also includes a short introduction to Docker Compose and
demonstrates its usage over a simple use case.


--------
Material
--------

Training material consists of 8 `notebooks <https://gitlab.inria.fr/formations/docker/handsondocker/-/tree/master/notebooks>`_ prepared using
Jupyter notebooks and corresponding `exercises <https://gitlab.inria.fr/formations/docker/handsondocker/-/tree/master/exercises>`_. 

-------------
Prerequisites
-------------

Programming experience is necessary to be able to follow the training. The
examples contain simple Python code and does not require specific Python
knowledge.

It is preferable to use a Linux platform.

------------
Installation
------------

Requirements
------------

- `Docker Engine <https://docs.docker.com/>`_ (version >=20.10)

- `Compose plugin <https://docs.docker.com/compose/>`_ (version >=2.14)
  
**Docker Engine** and **Compose plugin** installations are necessary to run Docker
daemon on localhost and execute Docker client and Compose commands.

Docker Engine is only available for Linux platforms.

For Windows and Mac, Docker can be used through `Docker Desktop
<https://docs.docker.com/desktop/>`_. Commercial use of Docker Desktop requires
paid subscription under certain conditions.


*This training will not cover Docker Desktop.*

**Installation**

- **Docker Engine**

Please follow the installation instructions for your Linux distribution on
`Docker Engine installation overview <https://docs.docker.com/engine/install/>`__ **Server** section.

-  **Compose plugin**

Please follow the installation instructions on `Scenario two: Install the Compose plugin <https://docs.docker.com/compose/install/>`__.

Optional requirements
---------------------

- `Jupyter <https://jupyter.org/>`_

- `bash\_kernel <https://github.com/takluyver/bash_kernel>`__

This tutorial is prepared using Jupyter notebooks. Use of **Jupyter
Notebook/JupyterLab** together with **bash_kernel** provides an easy to use and
coherent interface. JupyterLab is favored over Jupyter Notebook as it provides
a tabbed work area where you can view notebook, terminal and text files side by side.

However use of **Jupyter Notebook/JupyterLab** and **bash kernel** is not
mandatory. It is possible to view contents of the notebooks `online
<https://formations.gitlabpages.inria.fr/docker/handsondocker/>`__ and follow
the instructions on your local Terminal command line.

**Installation**

The optional requirements can be installed using either the `environment.yml <./environment.yml>`_ or the `requirements.txt <./requirements.txt>`_ files.

- **environment.yml**

  Create **handsondocker** conda environment:

  .. code:: bash

      conda-env create -f environment.yml

  Activate environment:

  ::

      conda activate handsondocker

After running these commands *handsondocker* conda environment is created and ready to run the notebooks.
      
- **requirements.txt**

  Create **.handsondocker** virtual environment:

  .. code:: bash

      python -m venv .handsondocker

  Activate environment:

  ::

      source .handsondocker/bin/activate

  Install the dependencies:

  ::

     pip install -r requirements.txt

After running these commands *.handsondocker* virtual  environment is created and
ready to run the notebooks.


---------------------
Use of notebooks
---------------------

- If you do not prefer to install JupyterLab, you can follow the notebooks
  `online <https://formations.gitlabpages.inria.fr/docker/handsondocker/>`_
  and run all commands on your terminal from project's root directory.

- If you installed JupyterLab, run the command:

  ::

      jupyter lab

  from the project's root directory.
  
  This will open Jupyter Lab interface in the browser. You can navigate to *notebooks*
  directory from the file browser on the left.


  The initial introduction notebook

  - `0_Introduction.ipynb <./notebooks/0_Introduction.ipynb>`_

  introduces basic JupyterLab instructions that will be useful throughout
  the training.

  There are 7 notebooks on Docker:
  
  - `1_Docker_client_daemon.ipynb <./notebooks/1_Docker_client_daemon.ipynb>`_
  - `2_Dockerfile_Images_Containers.ipynb <./notebooks/2_Dockerfile_Images_Containers.ipynb>`_
  - `3_More_on_Dockerfile.ipynb <./notebooks/3_More_on_Dockerfile.ipynb>`_
  - `4_Build_Cache.ipynb <./notebooks/4_Build_Cache.ipynb>`_
  - `5_Data_Management.ipynb <./notebooks/5_Data_Management.ipynb>`_
  - `6_Networking.ipynb <./notebooks/6_Networking.ipynb>`_
  - `7_Docker_compose.ipynb <./notebooks/7_Docker_compose.ipynb>`_

  
  You can click on any notebook to view its contents.
  
-------
License
-------

"Hands-on Docker" training material by Hande Gözükan is licensed under
`Creative Commons Attribution-ShareAlike 4.0 International License <https://creativecommons.org/licenses/by-sa/4.0/legalcode>`_.

Some of the explanation parts of this material contains modified extracts from
`docker documentation <https://github.com/docker/docker.github.io>`_. These
parts are indicated with ``*`` in the material and licensed under
`Apache 2.0 license <https://www.apache.org/licenses/LICENSE-2.0.html>`_. You
can see copyright notice in `LICENSE_AL2 <./LICENSE_AL2>`_ file.

